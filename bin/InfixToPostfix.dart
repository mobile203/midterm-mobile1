import 'dart:io';
import 'Tokenizing.dart';

void main(List<String> args) {
  print("Enter word in program : ");
  String? input = stdin.readLineSync();
  List<String> tk = tokenizing(input);
  print("Infix to split -> $tk");
  List<String> pf = infix_postfix(tk);
  print("Infix to postfix -> $pf");
}

tokenizing(var m) {
  List<String> tkl = [];
  tkl = m.split(' ');
  int tkl_length = tkl.length;
  for (int i = 0; i < tkl_length; i++) {
    tkl.remove('');
  }
  return tkl;
}

infix_postfix(var tk) {
  List<String> operator = [];
  List<String> postfix = [];

  int precedence, last_precedence = 0;
  for (int i = 0; i < tk.length; i++) {
    if (tk[i] == "0" ||
        tk[i] == "1" ||
        tk[i] == "2" ||
        tk[i] == "3" ||
        tk[i] == "4" ||
        tk[i] == "5" ||
        tk[i] == "6" ||
        tk[i] == "7" ||
        tk[i] == "8" ||
        tk[i] == "9") {
      postfix.add(tk[i]);
    }
    if (tk[i] == "+" || tk[i] == "-" || tk[i] == "*" || tk[i] == "/") {
      if (tk[i] == "(" || tk[i] == ")") {
        precedence = 0;
      } else if (tk[i] == '+' || tk[i] == '-') {
        precedence = 1;
      } else if (tk[i] == '*' || tk[i] == '/') {
        precedence = 2;
      } else {
        precedence = 3;
      }
      if (operator.length > 0) {
        if (operator.last == "(" || operator.last == ")") {
          last_precedence = 0;
        } else if (operator.last == "+" || operator.last == "-") {
          last_precedence = 1;
        } else if (operator.last == "*" || operator.last == "/") {
          last_precedence = 2;
        } else {
          last_precedence = 3;
        }
      }
      while (operator.isNotEmpty &&
          operator.last != "(" &&
          precedence <= last_precedence) {
        String collect_oprt = operator.last;
        operator.removeLast();
        postfix.add(collect_oprt);
      }
      operator.add(tk[i]);
    }
    if (tk[i] == "(") {
      operator.add(tk[i]);
    }
    if (tk[i] == ")") {
      while (operator.last != "(") {
        String collect_oprt = operator.last;
        operator.removeLast();
        postfix.add(collect_oprt);
      }
      operator.remove("(");
    }
  }
  while (operator.isNotEmpty) {
    String collect_oprt = operator.last;
    operator.removeLast();
    postfix.add(collect_oprt);
  }

  return postfix;
}
