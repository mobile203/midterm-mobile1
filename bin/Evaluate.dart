import 'dart:io';
import 'dart:math';
import 'InfixToPostfix.dart';
import 'Tokenizing.dart';

evaluate_postfix(var postfix) {
  List<double> values = [];
  double number;

  for (int i = 0; i < postfix.length; i++) {
    if (postfix[i] == "0" ||
        postfix[i] == "1" ||
        postfix[i] == "2" ||
        postfix[i] == "3" ||
        postfix[i] == "4" ||
        postfix[i] == "5" ||
        postfix[i] == "6" ||
        postfix[i] == "7" ||
        postfix[i] == "8" ||
        postfix[i] == "9") {
      double num = double.parse(postfix[i]);
      values.add(num);
      switch (postfix[i]) {
      }
    } else {
      double right;
      right = values.last;
      values.removeLast();
      double left;
      left = values.last;
      values.removeLast();
      double sum = 0;

      if (postfix[i] == "+") {
        sum = left + right;
      }
      if (postfix[i] == "-") {
        sum = left - right;
      }
      if (postfix[i] == "*") {
        sum = left * right;
      }
      if (postfix[i] == "/") {
        sum = left / right;
      }
      if (postfix[i] == "^") {
        sum = 0.0 + pow(left, right);
      }
      values.add(sum);
    }
  }
  return values[0];
}

void main() {
  print("Enter word in program : ");
  String? input = stdin.readLineSync();
  List<String> tk = tokenizing(input);
  print("Infix to split -> $tk");
  List<String> pf = infix_postfix(tk);
  print("Infix to postfix -> $pf");
  double a = evaluate_postfix(pf);
  print("Postfix to sum -> $a");
}
